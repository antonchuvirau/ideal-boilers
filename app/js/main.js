// Main scripts for the project
'use strict';
! function () {
    $(document).ready(function () {
        //Variables
        let sliderIndex;
        //Slider
        let slider = new Swiper('.ib-steps-slider', {
            slidesPerView: 1,
            allowTouchMove: false,
            passiveListeners: false,
            preventInteractionOnTransition: true,
            // effect: 'fade',
            autoHeight: true,
            // fadeEffect: {
            //     crossFade: true
            // }
        });
        let sectionsArray = ['home', 'fuel', 'hwc', 'position', 'flue', 'beds', 'baths'];
        //Breadcrumbs carousel
        let breadcrumbsCarousel = new Swiper('.ib-modal__breadcrumbs', {
            slidesPerView: 'auto',
            // Responsive breakpoints
            // breakpoints: {
            //     // when window width is >= 320px
            //     320: {
            //         slidesPerView: 'auto',
            //         centeredSlides: true
            //     },
            //     992: {
            //         slidesPerView: 'auto',
            //         centeredSlides: false
            //     },
            //     // when window width is >= 1220px
            //     1220: {
            //         slidesPerView: 'auto',
            //         simulateTouch: false
            //     }
            // }
        });
        $(window).on('resize', function(){
            setTimeout(function(){
                breadcrumbsCarousel.update();
            }, 300);
        });
        //Modal
        $('[data-modal]').click(function () {
            $($(this).data('modal')).modal();
            return false;
        });
        //Range slider
        let rangeSliderDeposit = $('.ib-range-slider__deposit').slider({
            value: $('.ib-range-slider__deposit').data('value'),
            min: $('.ib-range-slider__deposit').data('min'),
            step: $('.ib-range-slider__deposit').data('step'),
            max: $('.ib-range-slider__deposit').data('max'),
            slide: function (event, ui) {
                $('.ib-range-slider__deposit').prev().find('span').html(ui.value);
            }
        });
        let rangeSliderPeriod = $('.ib-range-slider__period').slider({
            value: $('.ib-range-slider__period').data('value'),
            min: $('.ib-range-slider__period').data('min'),
            step: $('.ib-range-slider__period').data('step'),
            max: $('.ib-range-slider__period').data('max'),
            slide: function (event, ui) {
                $('.ib-range-slider__period').prev().find('span').html(ui.value);
            }
        });
        //Events
        $('#ib-modal').on($.modal.OPEN, function () {
            slider.update();
        });
        $('.ib-finance-options__btn').on('click', function () {
            $('.ib-finance-options').toggle();
        });
        slider.on('slideChangeTransitionStart', function () {
            sliderIndex = slider.realIndex;
            changePaginationStyle(sliderIndex);
        });
        //Agreement checkbox
        let agreementInputElement = document.querySelector('.ib-checkbox__input');
        agreementInputElement.addEventListener('click', function(e){
            if (this.checked){
                this.closest('.ib-form__grid').querySelector('button[type="submit"]').removeAttribute('disabled');
            }
            else{
                this.closest('.ib-form__grid').querySelector('button[type="submit"]').setAttribute('disabled', 'disabled');
            }
        });
        $('.ib-choice-radio__input').on('click', function () {
            if ($(this).is(':checked')) {
                setTimeout(function () {
                    let activeSlideIndex = getActiveSlideIndex();
                    let choiceInputs = $('.ib-steps-slider__item').eq(activeSlideIndex).find('.ib-choice-radio__input');
                    choiceInputs.each(function () {
                        if ($(this).is(':checked')) {
                            generateModalBreadcrumbs(activeSlideIndex, $(this).val());
                        }
                    });
                    moveSlider();
                    setActiveDotClass(activeSlideIndex + 1);
                }, 300);
            }
        });
        $('.ib-product-item__input').on('click', function () {
            if ($(this).is(':checked')) {
                setTimeout(function () {
                    let activeSlideIndex = getActiveSlideIndex();
                    moveSlider();
                    generateBackBtn(activeSlideIndex);
                });
            }
        });
        $(document).on('click', '.ib-modal__btn', function () {
            slider.slidePrev();
            $(this).remove();
            $('.ib-modal__breadcrumbs-btn').removeClass('ib-modal__breadcrumbs-btn_hidden');
            breadcrumbsCarousel.update();
            moveTo();
        });
        $(document).on('click', '.ib-modal__breadcrumbs-btn', function () {
            let currentBtnIndex = $(this).closest('.ib-modal__breadcrumbs .swiper-wrapper').children().index($(this));
            slider.slideTo(currentBtnIndex);
            removeActiveDotClass(currentBtnIndex);
            removeBreadcrumbsnBtn(currentBtnIndex);
            breadcrumbsCarousel.update();
            moveTo();
        });
        $('.ib-form__btn').on('click', function(e){
            e.preventDefault();
            moveSlider();
        });
        //Functions
        function moveSlider() {
            slider.slideNext();
        }

        function getActiveSlideIndex() {
            return slider.realIndex;
        }

        function removeActiveDotClass(index) {
            let dotIndex = index;
            $('.ib-pagination__dot').each(function (index) {
                if (index > dotIndex && index !== 0) {
                    $(this).removeClass('ib-pagination__dot_active');
                }
            });
            return;
        }

        function moveTo(){
            let breadcrumbsCarouselSlidesLength = breadcrumbsCarousel.slides.length;
            breadcrumbsCarousel.slideTo(breadcrumbsCarouselSlidesLength);
            return;
        }

        function setActiveDotClass(index) {
            $('.ib-pagination__dot').eq(index).addClass('ib-pagination__dot_active');
            return;
        }

        function changePaginationStyle(index) {
            if (index === 7) {
                $('.ib-pagination__item').eq(0).find('.ib-pagination__line').toggleClass('ib-pagination__line_style-custom ib-pagination__line_style-dots-no');
                $('.ib-pagination__item').eq(0).find('.ib-pagination__circle').addClass('ib-pagination__circle_icon-completed');
                $('.ib-pagination__item').eq(1).addClass('ib-pagination__item_opened ib-pagination__item_active');
                $('.ib-pagination__item').eq(2).addClass('ib-pagination__item_opened').removeClass('ib-pagination__item_active');
                $('.ib-pagination__item').eq(3).addClass('ib-pagination__item_opened');
            }
            if (index === 8) {
                $('.ib-pagination__item').eq(2).addClass('ib-pagination__item_active');
            }
            if (index < 7) {
                $('.ib-pagination__item').eq(0).find('.ib-pagination__line').removeClass('ib-pagination__line_style-dots-no');
                $('.ib-pagination__item').eq(0).find('.ib-pagination__line').addClass('ib-pagination__line_style-custom');
                $('.ib-pagination__item').eq(0).find('.ib-pagination__circle').removeClass('ib-pagination__circle_icon-completed');
                $('.ib-pagination__item:not(:first-child)').removeClass('ib-pagination__item_active ib-pagination__item_opened');
            }
            if (index === 9){
                $('.ib-modal__pagination').addClass('ib-pagination_visibility-hidden');
                $('.ib-modal__breadcrumbs').addClass('ib-modal__breadcrumbs_visibility-hidden');
            }
            else{
                $('.ib-modal__pagination').removeClass('ib-pagination_visibility-hidden');
                $('.ib-modal__breadcrumbs').removeClass('ib-modal__breadcrumbs_visibility-hidden');
                breadcrumbsCarousel.update();
            }
            return;
        }

        function removeBreadcrumbsnBtn(index) {
            let btnIndex = index;
            $('.ib-modal__breadcrumbs-btn').each(function (index) {
                if (index + 1 > btnIndex) {
                    $(this).remove();
                }
            });
            breadcrumbsCarousel.update();
            if (window.innerWidth < 992){
                moveTo();
            }
            return;
        }

        function generateModalBreadcrumbs(index, value) {
            let item = '<button type="button" class="ib-modal__breadcrumbs-btn swiper-slide"><span>' + value + '</span>' + sectionsArray[index] + '</button>';
            $('.ib-modal__breadcrumbs').find('div.swiper-wrapper').append(item);
            breadcrumbsCarousel.update();
            if (window.innerWidth < 992){
                moveTo();
            }
            return;
        }

        function generateBackBtn(index) {
            let leftArrowTemplate = '<svg width="11" height="18" viewBox="0 0 11 18" fill="none" xmlns="http://www.w3.org/2000/svg">' +
                '<path d="M10 1L2 9L10 17" stroke="#C42926" stroke-width="2"/>' +
                '</svg>';
            if (index === 7) {
                $('.ib-modal__breadcrumbs').addClass('ib-modal__breadcrumbs_layout-custom');
                $('.ib-modal__breadcrumbs-btn').addClass('ib-modal__breadcrumbs-btn_hidden');
                $('.ib-modal__breadcrumbs .swiper-wrapper').append('<button type="button" class="ib-btn swiper-slide ib-btn_arrow-left ib-modal__btn">' + leftArrowTemplate + 'Back</button>');
                breadcrumbsCarousel.update();
            }
            return;
        }
    });
}();